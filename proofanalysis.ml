open Printf

let all_exported = Export.list_the_exported_thms ();;
let all_search = (search []);;
let all_thms = fst (List.split all_exported);;

let all_1000 = List.filter (fun x -> if (obtain_proof_size x) < 59800
							 then true else false) all_thms;;


let all_500 = List.filter (fun x -> if (obtain_proof_size x) < 6910
							 then true else false) all_thms;;

let c_i_size_compared thms = 
	let l1 = List.map detect_LEM thms in 
	let l2 = List.map obtain_proof_size thms in 
	let f ((c_count, i_count), (c_size_count, i_size_count)) t size = 
		if t then ((c_count +1, i_count),(c_size_count + size, i_size_count))
		else ((c_count , i_count + 1),(c_size_count, i_size_count + size)) in 
	List.fold_left2 f ((0,0),(0,0)) l1 l2;;


let get_statistics_size thms =
	let  ()  = Printf.printf "%s \n" "getting statistics"  in 
	let () =  print_newline () in 
	let ((c_count, i_count), (c_size_count, i_size_count)) = c_i_size_compared thms in 
	let  ()  = Printf.printf "%s \n" "got statistics"  in 
	let () = Printf.printf "c_size_count %s \n" (Printf.sprintf "%d" c_size_count) in 
	let () = Printf.printf "i_size_count %s \n" (Printf.sprintf "%d" i_size_count) in 
	let () =  print_newline () in 
	let i_size_count' = float_of_int i_size_count in 
	let c_size_count' = float_of_int c_size_count in 
	let c_count' = float_of_int c_count in 
	let i_count' = float_of_int i_count in 
	if c_count == 0 && i_count == 0 then ((c_count, i_count), (0.0, 0.0)) 
	else if c_count = 0 then ((c_count, i_count), (0.0, (i_size_count' /. i_count'))) 
	else if i_count = 0 then ((c_count, i_count), ((c_size_count' /. c_count'),0.0)) 
	else ((c_count, i_count),((c_size_count' /. c_count'), (i_size_count' /. i_count')));;


let get_count_list thms amount = 
	let l1 = List.map detect_LEM thms in 
	let l2 = List.map obtain_proof_size thms in 
	(* get upper bound of list *)
	let maxi acc e =
		if acc > e then acc else e in 
	let max = List.fold_left maxi (List.hd l2) l2 in 
	(* get lower bound of list *)
	let mini acc e = 
		if acc < e then acc else e in 
	let min = List.fold_left mini (List.hd l2) l2 in 
	(* convert to float number *)
	let max' = float_of_int max in 
	let min' = float_of_int min in 
	let amount' = float_of_int amount in 
	let portion' = (max' -. min') /. amount' in 
	(* get a counting list of size amount (all zeros as initual value) *)
	let rec get_init_list acc count = 
		if count == 0 then acc
		else 
		get_init_list ((ref 0)::acc) (count - 1) in 
	let l' = get_init_list [] amount in (* classical *)
	let l'' = get_init_list [] amount in (* constructive *)
	(* a recursive function to count through the whole list *)
	let ct (lc, li) t e = 
		let e' = float_of_int e in 
		let index = int_of_float (log10 (e' -. min')) in 
		(* let () = Printf.printf "%d at %d\n" e index in  *)
		if t then 
			(* classical  *)
			let lc = 
				if index = (List.length lc) 
				then 
					let () = (List.nth lc (index-1)) := !(List.nth lc (index-1)) + 1 in 
					lc
				else 
					let () = (List.nth lc index) := !(List.nth lc index) + 1 in 
					lc
				in
			(lc, li)
		else 
			(* classical  *)
			let li = 
				if index = (List.length li) 
				then 
					let () = (List.nth li (index-1)) := !(List.nth li (index-1)) + 1 in 
					li
				else 
					let () = (List.nth li index) := !(List.nth li index) + 1 in 
					li
				in
			(lc, li)
		in
	let (l', l'') = List.fold_left2 ct (l', l'') l1 l2 in 
	let lc = List.map (fun x -> !x) l' in 
	let li = List.map (fun x -> !x) l'' in 
	(min, max, lc, li);;

let get_barchart thms = 
	let () = Printf.printf "" in
	let () = print_newline () in 
	let (min, max, lc, li) = get_count_list thms 8 in 
	let () = Printf.printf "The min is %d and the max is %d\n" min max in 
	(* let () = Printf.printf "The first is %d \n" (List.nth l 0) in  *)
	List.iter (fun x -> Printf.printf "classical %d\n" x) lc ;
	List.iter (fun x -> Printf.printf "constructive %d\n" x) li ;;
(* 

get_barchart all_1000;; 
List.length all_1000;;
get_statistics_size all_1000;; 
 *)

(* List.length all_500;;
get_statistics_size all_thms;;  *)


let rec rob_print_thm (space: int) (phyp:bool) thm = 

  let gap = space + 7 in 

  let print_gap g = let n = (g-4) in if n >= 0 then Printf.printf "%s" (String.make n ' '); Printf.printf "+--" in 

  let print_gap2 g = let n = (g-4) in if n >= 0 then Printf.printf "%s" (String.make n ' '); Printf.printf "`--" in 

  let () = print_gap space in

  (* obtain hyp, concl and its proof*)

  let h = hyp thm in

  let c = concl thm in 

  let p = read_proof thm in 

  (* print concl and hyp *)

  let () = Printf.printf "%s -| " (string_of_term c) in 

  let () = if phyp then List.iter (Printf.printf "<%s> ") (List.map string_of_term h) ; Printf.printf "\n" in 

  let () = print_gap2 space in

  match p with 

  | Axiom_proof | Lemma_proof(_)-> Printf.printf "[%s] \n" " Axiom/Lemma "

  | Lem_proof -> Printf.printf "[%s] \n" "  Lem  "

  | Refl_proof(term) -> Printf.printf "[%s] \n" "  Refl "

  | Sym_proof(th1) -> Printf.printf "[%s] \n" "  Sym  " ; rob_print_thm gap phyp th1 

  | Trans_proof (th1 , th2) -> Printf.printf "[%s] \n" " Trans " ; rob_print_thm gap phyp th1 ; rob_print_thm gap phyp th2

  | Mk_comb_proof (th3, th4) -> Printf.printf "[%s] \n" "Mk_comb" ; rob_print_thm gap phyp th3 ; rob_print_thm gap phyp th4

  | Abs_proof (term, th) -> Printf.printf "[%s] \n" "  Abs  "; rob_print_thm gap phyp th

  | Beta_conv_proof(term) -> Printf.printf "[%s] \n" " BetaC "; print_gap2 gap; Printf.printf "%s \n" (string_of_term term);

  | Assume_proof (term) -> Printf.printf "[%s] \n" "Assume "; print_gap2 gap; Printf.printf "%s \n" (string_of_term term);

  | Eq_mp_proof (th5, th6) -> Printf.printf "[%s] \n" " Eq_mp "; rob_print_thm gap phyp th5 ; rob_print_thm gap phyp th6

  | Deduct_antisym_rule_proof (th7, th8) -> Printf.printf "[%s] \n" "DeductA"; rob_print_thm gap phyp th7 ; rob_print_thm gap phyp th8

  | Prove_hyp_proof (th9, th10) -> Printf.printf "[%s] \n" "ProveHP"; rob_print_thm gap phyp th9 ; rob_print_thm gap phyp th10

  | Subst_proof(_, th1) -> Printf.printf "[%s] \n" "Subst"; rob_print_thm gap phyp th1

  | New_basic_definition_proof(_) -> Printf.printf "[%s] \n" " New_BD"

  | New_basic_type_definition_proof (_) -> Printf.printf "[%s] \n" "New_BTD"

  | Define_const_list_proof(_)-> Printf.printf "[%s] \n" "DefinCL";;



let easy_print thm = 
	rob_print_thm 0 true thm;;





(* get_barchart all_thms;;  *)

let () = Printf.printf "subst %d\n" !subst_count;;
let () = Printf.printf "eqmp %d\n" !eqmp_count;;
let () = Printf.printf "appthm %d\n" !appthm_count;;
let () = Printf.printf "proveHyp %d\n" !proveHyp_count;;
let () = Printf.printf "betaConv %d\n" !betaConv_count;;
let () = Printf.printf "absThm %d\n" !absThm_count;;
let () = Printf.printf "trans %d\n" !trans_count;;
let () = Printf.printf "refl %d\n" !refl_count;;
let () = Printf.printf "deductAntisym %d\n" !deductAntisym_count;;
let () = Printf.printf "sym %d\n" !sym_count;;
let () = Printf.printf "assume %d\n" !assume_count;;
let () = Printf.printf "mp %d\n" !mp_count;;
let () = Printf.printf "disch %d\n" !disch_count;;
let () = Printf.printf "gen %d\n" !gen_count;;
let () = Printf.printf "spec %d\n" !spec_count;;

let total_count = ( !subst_count +  !eqmp_count + 
!appthm_count + !proveHyp_count +  !betaConv_count + !absThm_count + 
 !trans_count + !refl_count + !deductAntisym_count + !sym_count + 
 !assume_count + !mp_count + !disch_count + !gen_count + !spec_count);;

let () = Printf.printf "Count: %d\n"  total_count;;




(* -------- Proof size of basic deduction rules --------- *)
(* let th1 = ASSUME `p:bool`;;
let th2 = ASSUME `p ==> q`;;
let th3 = MP th2 th1;;

let mp_size = get_proof_size th3;;

let th4 = DISCH `q:bool` th1;;
let disch_size = get_proof_size th4;;

let th5 = GEN `x:A` th1 ;;
let gen_size = get_proof_size th5;;

let th6 = ASSUME `!x:A. p` ;;
let th7 = SPEC `q:A` th6 ;;
let spec_size = get_proof_size th7;;

Printf.printf "Mp : %d\nDisch : %d \nGen : %d\nSPEC : %d\n" mp_size disch_size gen_size spec_size;;

let eqt_intro_size = get_proof_size (EQT_INTRO th1);;

let th8 = ASSUME `q:bool`;;
let th9 = CONJ th1 th8;;

let conj_size = get_proof_size th9;;

let th10 = ASSUME `q ==> p` ;;
let th11 = IMP_ANTISYM_RULE th2 th10;;

let imp_anti_sym_size = get_proof_size th11;;

let th12 = DISJ1 th1 `q:bool`;;
let disj1_size = get_proof_size th12 ;;

Printf.printf "EQT_INTRO: %d\n CONJ %d\n IMP_ANTI_SYM %d\nDISJ1 %d\n" eqt_intro_size conj_size imp_anti_sym_size disj1_size;;


let taut_size = get_proof_size (TAUT `!p. p \/ ~p`);;
Printf.printf "TAUT size : %d \n" taut_size;; 
 *)


(* %----------------------------------- *)




let print_size name th = 
	let () = start_logging() in 
	let  () = logfile "tmp_count_size" in  
	let () = subst_count := 0 in 
    let () = eqmp_count := 0 in 
	let () = appthm_count := 0 in 
	let () = proveHyp_count := 0 in 
	let () = betaConv_count := 0 in 
	let () = absThm_count := 0 in 
	let () = trans_count := 0 in 
	let () = refl_count := 0 in 
	let () = deductAntisym_count := 0 in 
	let () = sym_count := 0 in 
	let () = assume_count := 0 in 
	let () = mp_count := 0 in 
	let () = disch_count := 0 in 
	let () = gen_count := 0 in 
	let () = spec_count := 0 in 
	(* --------------------------------- *)

	let  () = export_thm th in 
	(* let spec_size = get_proof_size th7;; *)

	(* --------------------------------- *)
	let total_count = ( !subst_count +  !eqmp_count + 
	!appthm_count + !proveHyp_count +  !betaConv_count + !absThm_count + 
	 !trans_count + !refl_count + !deductAntisym_count + !sym_count + 
	 !assume_count + !mp_count + !disch_count + !gen_count + !spec_count) in 

	let () = Printf.printf "%s Count: %d\n" name total_count in 
	logfile_end ();;


let th1 = ASSUME `p:bool`;;
let th2 = ASSUME `p ==> q`;;
let th3 = MP th2 th1;;

(* let mp_size = get_proof_size th3;; *)
print_size "mp" th3;;

let th4 = DISCH `q:bool` th1;;
(* let disch_size = get_proof_size th4;; *)

print_size "disch" th4;;

let th5 = GEN `x:A` th1 ;;
(* let gen_size = get_proof_size th5;; *)

print_size "gen" th5;;

let th6 = ASSUME `!x:A. p` ;;
let th7 = SPEC `q:A` th6 ;;
(* let spec_size = get_proof_size th7;; *)
print_size "spec" th7;;

(* Printf.printf "Mp : %d\nDisch : %d \nGen : %d\nSPEC : %d\n" mp_size disch_size gen_size spec_size;; *)


(* let eqt_intro_size = get_proof_size (EQT_INTRO th1);; *)

(* print_size "eqt_intro" (EQT_INTRO th1);; *)

let th8 = ASSUME `q:bool`;;
let th9 = CONJ th1 th8;;

(* let conj_size = get_proof_size th9;; *)

print_size "conj" th9;;



let thdis = DISJ1 th1 `w:bool`;;
print_size "disj1" thdis;;




let th10 = ASSUME `q ==> p` ;;
let th11 = IMP_ANTISYM_RULE th2 th10;;

(* let imp_anti_sym_size = get_proof_size th11;; *)

print_size "imp_anti_sym " th11;;


(* let taut_size = get_proof_size (TAUT `!p. p \/ ~p`);;
Printf.printf "TAUT size : %d \n" taut_size;;  *)

print_size "tautLEM" (TAUT `!p. p \/ ~p`);;

