(* #use "hol.ml";; *)

start_logging ();;
logfile "test_rob";;


let thm1 = ASSUME `p:bool ==> q:bool`;;
let thm2 = ASSUME `p:bool`;;
let thm3 = MP thm1 thm2;;
export_thm thm1;;
export_thm thm2;;
export_thm thm3;;

let th = REFL `p:bool`;;
export_thm th;;

let th1 = ASSUME `p:bool = q:bool`	;;
let th2 = TRANS th th1;;
export_thm th2;;

let th3 = CONJUNCT2(ASSUME `p /\ q /\ r`)
    and th4 = CONJUNCT2(ASSUME `q /\ r`);;

let th5 = PROVE_HYP th3 th4;;
export_thm th5;;

let thm4 = DISCH `p:bool` thm2;;
export_thm thm4;;

let thm5 = ASSUME `!x:bool. (x \/ T)`;;
export_thm thm5;;

let thm6 = SPEC `T` thm5;;
export_thm thm6;;

let thm7 = GEN `x` thm6;;
export_thm thm7;;


(* let myLEM = 
	prove (`p:bool \/ ~p`,
	MESON_TAC []);;

export_thm myLEM;;
 *)

logfile_end ();;
stop_logging ();;
